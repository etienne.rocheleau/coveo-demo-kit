import { LightningElement, wire, track, api } from "lwc";
import { getRecord } from "lightning/uiRecordApi";

import Id from "@salesforce/user/Id";
import isGuest from "@salesforce/user/isGuest";

const COLUMNS = [
  {
    label: "Field Name",
    fieldName: "field"
  },
  {
    label: "Value",
    fieldName: "value"
  }
];

export default class UserInfos extends LightningElement {
  @track data = [];
  columns = COLUMNS;

  /**
   * Contains all fields passed through the visit information panel with a prefix of "User."
   * i.e. ["User.Name", "User.Locale"]
   */
  _fields = [];

  @api
  get fields() {
    return this._fields.join(";").replace(/User./g, "");
  }

  set fields(fields) {
    this._fields = fields.split(";").map(field => "User." + field);
  }

  @wire(getRecord, { recordId: Id, fields: "$_fields" })
  wireUserInfo({ error, data }) {
    if (isGuest) {
      this.data = [{ field: "Name", value: "Guest" }];
      return;
    }

    if (error) {
      console.error(
        `An error occurred obtaining data for the following fields: ${JSON.stringify(
          this._fields
        )}`
      );
      return;
    }
    if (!data) {
      console.log(
        `No CRM data obtained for the following fields: ${JSON.stringify(
          this._fields
        )}`
      );
      return;
    }
    this.parseUserData(data);
  }

  parseUserData(data) {
    try {
      this.data = this.fields.split(";").map(field => {
        return { field, value: data.fields[field].value };
      });
    } catch (ex) {
      console.error("Error parsing data");
      console.error(ex.message);
    }
  }
}