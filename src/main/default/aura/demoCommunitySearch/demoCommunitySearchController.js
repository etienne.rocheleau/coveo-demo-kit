({
  searchInitialized: function (cmp, event, helper) {
    helper.bindEvents(cmp);
  },
  handleStartFlow: function (cmp, event) {
    const _component = cmp;
    const flowApiName = event.getParam('flowAPIName');
    if (flowApiName) {
      let modalBody;
      $A.createComponent("c:CustomFlowLauncher", {
        'flowApiName': flowApiName,
        'finishedHandler': $A.getCallback(function(output) {
          _component.get('v.modalPromise').then(function(modal) {
            if(modal) {
              modal.close();
            }
          });
        })
      },
      function (content, status, message) {
        if (status === "SUCCESS") {
          modalBody = content;
          const modalPromise = cmp.find('overlayLib').showCustomModal({
            body: modalBody,
            showCloseButton: true,
            cssClass: "mymodal"
          });
          _component.set('v.modalPromise', modalPromise);
        } else {
          console.error(message);
        }
      });
    }
  }
})