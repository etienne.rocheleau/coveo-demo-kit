# Coveo Demo Kit (CDK)

The coveo demo kit contains and assortment of components that are frequently used when building custom demos for clients. The goal for this project is
to decrease the amount of time spent on either building the same components used in demos, or having to find old code to copy which can result in
a messy PoC with hidden issues resulting in countless debugging hours.

## Installing the CDK

To install the cove demo kit ensure that the salesforce organization already has coveo for salesforce package installed. The version of coveo for
salesforce that is required is can be found in the `sfdx-project.json`, found at the root of this repository. Once the coveo for salesforce package
is installed the CDK can be installed by the following command: 

`sfdx force:package:install --package <PACKAGE_ID> --targetusername <USERNAME> --noprompt --wait 10`

where:

  - PACKAGE_ID:  The current package ID for the coveo demo kit. (Details to follow)
  - USERNAME: The username or alias for the target salesforce org. 

Currently the package id can be obtained by request.

## Feature/Component Requests

To request a feature or component a ticket can be opened on the Demos project on jira. The ticket should be tagged as a "Request" and the
description should be clear and detailed. 

## Developing for the CDK

Currently there are no style or best practices for developing features within the CDK however the core concept should be to create features/components that provide re-usability with configurable properties. Therefore creating a super specific component that can be used in a niche situation should be avoided.

### Development Flow

Once a Jira ticket is created as either a request or improvement the developer should appropriately assign the ticket to themselves. Once the 
work is ready to begin create a branch associated to the Jira ticket (i.e. DEM-#). Once the work is completed the developer can open a merge
request and assign the appropriate developers to the MR. This will also trigger gitlab pipelines that will create the CDK package and install it 
to a scratch org. This allows the dev to test their changes in a scratch org. 

### Manual Testing

Once a PR is created a pipeline will run, the results of the pipeline will create artifact containing the package ID. The package ID will also
be displayed to the console output. This package can now be installed to an org to test the changes made by the developer. Use the same command
as if you were installing the official package. **NOTE** it is good practice to test these intermediate packages within a scratch org.